<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet
  version="3.0" expand-text="yes"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:file="http://expath.org/ns/file"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:css="http://words.barefootliam.org/xml/css/ns"
  xmlns:map="http://www.w3.org/2005/xpath-functions/map"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="#all"
  extension-element-prefixes="css"
  >

  <!--* A simple test file.
      *
      * This can be run using Saxon-PE or -EE (but i think not HE).
      *
      * XSLT-only implementation:
      * saxon-ee test-input.xml test.xsl > test-output.html
      * You should also end up with test-output.css
      *
      * JAVA + XSLT implementation:
      *
      * Java-only:
      *
      *-->

  <xsl:param name="java-compiletime" as="xs:boolean" static="yes"
    select=" false() " />

  <xsl:param name="java-runtime" as="xs:boolean" static="yes"
    select=" false() " />

  <xsl:param name="boy-name" as="xs:string" select=" 'Simon' " />

  <xsl:param name="css:already-started" select=" 'no' " />

  <xsl:variable name="usejava" as="xs:boolean" static="yes"
    select="$java-compiletime or $java-runtime" />

  <xsl:import href="lib/css-hide.xsl" use-when="not($java-compiletime)" />
  <xsl:import href="lib/css-gather.xsl" use-when="not($java-runtime)" />

  <!--* JAVA runtime version: *-->
  <xsl:template match="/" use-when="$java-runtime">
    <xsl:apply-templates select="*" />
    <xsl:result-document method="text" href="test.css">
      <css:gather stream="test" use-when="$java-runtime" />
    </xsl:result-document>
  </xsl:template>

  <!--* XSLT runtime version: *-->
  <xsl:template match="document-node()[not($css:already-started='yes')]" use-when="not($java-runtime)">
    <xsl:variable name="here-hack" as="element(*)"><elephant /></xsl:variable>
    <xsl:variable name="top-level-stylesheet-uri"
      select="base-uri($here-hack)" />
    <xsl:variable name="top-level-stylesheet" use-when="not($java-runtime)"
      select="doc($here-hack)" />

    <xsl:choose>
      <xsl:when test="$java-compiletime">
	<!--* In this case all css:* elements were removed when
	    * the stylesheet was compiled, so we can ignore them
	    *-->
	<xsl:apply-templates select="*" />
      </xsl:when>

      <xsl:when test="css:already-started = 'yes'">
	<!--* don't call initial-template if we already did *-->
      </xsl:when>

      <xsl:otherwise>
	<xsl:call-template name="css:initial-template">
	  <xsl:with-param name="stylesheet" select="$top-level-stylesheet" />
	  <xsl:with-param name="stylesheet-uri"
	    select="$top-level-stylesheet-uri" />
	  <!--* pass any stylesheet parameters here as QNames/values: *-->
	  <xsl:with-param name="stylesheet-params"
	    select="map {
	      QName('', 'boy-name') : $boy-name
	    }" />
	</xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>

    <!--* now make the style sheet *-->
    <xsl:result-document method="text" href="test.css">

      <xsl:call-template name="css:gather" use-when="not($java-runtime)">
        <xsl:with-param name="stream" select=" 'test' " />
        <xsl:with-param name="stylesheet" select="$top-level-stylesheet/*" />
      </xsl:call-template>
    </xsl:result-document>
  </xsl:template>

  <xsl:template match="document-node()[$css:already-started='yes']" use-when="not($java-runtime)">
    <!--* this is reached via fn:transform *-->
    <html>
      <xsl:apply-templates />
    </html>
  </xsl:template>

  <xsl:template match="boy">
    <css:rule match="div.child" stream="test">
      color: red;
      font: serif;
    </css:rule>
    <css:media when="min-width: 700px" stream="test">
      <css:rule match="div.child">
        color: blue;
      </css:rule>
    </css:media>
    <h1 expand-text="yes">{$boy-name}</h1>
    <div class="child">
      <xsl:apply-templates/>
    </div><!--* child *-->
  </xsl:template>
</xsl:stylesheet>
