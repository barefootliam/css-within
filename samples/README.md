# CSS Within - Samples

There are two parts to running transforms that use CSS Within:

1.	dealing with the css elements in your XSLT files, so as
	not to have them interpreted as direct element constructors
	and ending up in the output;

2.	generating the CSS files.

The first of these can be done in three ways:

1.    pre-process your stylesheet to remove all elements in the css
      namespace and _either_ put the contents into _css:generate_ elements,
      _or_ put the content into separate CSS files;

2.    post-process the result of running your transformation,
      removing all the CSS elements;

3.    use the Java extension provided (or another one) to have the
      css elements removed during stylesheet compilation.

Generating the CSS can be done in two ways:

1.    using the pre-processor to gather all the CSS definitions into
      where css:generate elements were in the stylesheet,

2.    gather all the CSS rules by processing the stylesheet,
      and write them out, not using css:generate elements.


