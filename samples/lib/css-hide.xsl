<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet
  version="3.0" expand-text="no"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:file="http://expath.org/ns/file"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:css="http://words.barefootliam.org/xml/css/ns"
  xmlns:map="http://www.w3.org/2005/xpath-functions/map"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="#all"
  >

  <!--* CSS Within compile-time support:
      * rmee-run stylesheet using fn:transform() and
      * strip out css:* elements
      *-->

  <xsl:template name="css:initial-template">
    <xsl:param name="stylesheet" as="document-node()" />
    <xsl:param name="stylesheet-uri" as="xs:string" />
    <xsl:param name="stylesheet-params" as="map(*)?" select="()" />

    <xsl:sequence select='
      map:for-each(
	transform(
	  map {
	    "stylesheet-node" : css:remove-extensions($stylesheet/*) ,
	    "source-node" : .,
	    "post-proess" : css:writefile#2,
	    "delivery-format" : "document",
	    "stylesheet-params" : map:merge((map {
	      QName(
		"http://words.barefootliam.org/xml/css/ns",
		"already-started"
	      ) : "yes"
	    }, $stylesheet-params))
	  }
	),
	function(
	  $key as xs:anyAtomicType,
	  $value as item()*
	) as item()* {
          css:writefile($key, $value)
	}
      )
      ' />
  </xsl:template>

  <xsl:function name="css:remove-extensions" as="document-node()">
    <xsl:param name="input" as="element(*)" />
    <xsl:document>
      <xsl:variable name="other-extensions" as="xs:string"
        select="replace(' ' || $input/@extension-element-prefixes || ' ', ' css ', ' ')" />

      <xsl:copy select="$input">
	<xsl:copy-of select="$input/@*"/>
	<xsl:attribute name="extension-element-prefixes"
	  select="$other-extensions" />
	<xsl:copy-of select="$input/node()" />
      </xsl:copy>
    </xsl:document>
  </xsl:function>

 <xsl:function name="css:writefile" as="item()*">
    <xsl:param name="uri" as="xs:string"/>
    <xsl:param name="contents" as="item()*" />

    <xsl:variable name="cleaned" as="item()*">
      <xsl:choose>
	<xsl:when test="$java-compiletime" use-when="$java-compiletime">
	  <xsl:sequence select="$contents" />
	</xsl:when>
	<xsl:when test="false()">
	  <!--* at least one xsl:when is required *-->
	</xsl:when>
	<xsl:otherwise>
	  <xsl:apply-templates select="$contents" mode="css:remove-css" />
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="empty($uri) or ($uri eq '')">
        <xsl:sequence select=" $cleaned " />
      </xsl:when>
      <xsl:when test="$uri eq 'output'">
        <xsl:sequence select=" $cleaned " />
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="matches($uri, '.*[^/:]/[^/]+')">
          <!--* we need to create the directory *-->
          <xsl:variable name="dir" select="replace($uri, '/[^/]*$', '')"/>
          <xsl:variable name="result" select="file:create-dir($dir)" />
          <xsl:if test="$result">
            <xsl:message terminate="yes"
              select="concat('file:create-dir(', $dir, ') for ', $uri, ' failed: ', $result)"/>
          </xsl:if>
        </xsl:if>

        <xsl:value-of
          select="file:write-text(
            $uri,
            serialize(
              $cleaned,
              map {
                'html-version' : 5,
                'indent' : false(),
                'method': if (matches($uri, '\.html$')) then 'html' else 'text'
              }
          )
        )" />

      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:mode name="css:remove-css"  on-no-match="shallow-copy" />

  <xsl:template match="css:*" mode="css:remove-css" />

  <xsl:function name="css:remove-css">
    <xsl:param name="input" />

    <xsl:apply-templates mode="remove-css" select="$input" />
  </xsl:function>


</xsl:stylesheet>
