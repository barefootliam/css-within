<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet
  version="3.0" expand-text="no"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:file="http://expath.org/ns/file"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:css="http://words.barefootliam.org/xml/css/ns"
  xmlns:map="http://www.w3.org/2005/xpath-functions/map"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="#all"
  >

  <!--* CSS Within runtime support:
      * generate stylesheet
      *-->
  <xsl:mode name="css" on-no-match="shallow-skip" />

  <xsl:variable name="css:empty-string" as="xs:string"
    select="substring('##', 1, 0)" />

  <xsl:template name="css:gather" visibility="public" as="xs:string">
    <xsl:param  name="stream" as="xs:string" select=" '#all' " />
    <xsl:param name="stylesheet" as="element(*)" />
    <xsl:param name="indent"
      select="$css:empty-string" as="xs:string" tunnel="yes" />

    <xsl:variable name="result" as="xs:string*">

      <!--* You might want to have a global header, e.g. for @charset
	  * or for licence statements; although @stream is supported for
	  * consistency, the idea is @stream would not normally be used
	  *-->
      <xsl:value-of
	select="$stylesheet//css:global-header[
	  not(@stream) or (@stream = $stream)
	]" />

      <!--* now a header that can be stream-specific: *-->
      <xsl:value-of
	select="$stylesheet//css:header[
	  not(@stream) or ($stream eq '#all') or contains-token(@stream, $stream)
	]" />

      <xsl:value-of
	select="$stylesheet//css:header[
	  not(@stream) or ($stream eq '#all') or contains-token(@stream, $stream)
	]" />

      <!--* media rules (these are not currently allowed to nest) *-->
      <xsl:apply-templates mode="css:gather"
	select="$stylesheet//css:media[
	  not(@stream) or ($stream eq '#all') or contains-token(@stream, $stream)
	]" />

      <!--* now plain rules outside media rules *-->
      <xsl:apply-templates mode="css:gather"
	select="$stylesheet//css:rule[
	  not(@stream) or ($stream eq '#all') or contains-token(@stream, $stream)
	][
	  not(ancestor::css:media)
	]" />

      <!--* finally, a footer *-->
      <xsl:value-of
	select="$stylesheet//css:footer[
	  not(@stream) or ($stream eq '#all') or contains-token(@stream, $stream)
	]" />
    </xsl:variable>

    <xsl:sequence select="string-join($result, '')" />
  </xsl:template>

  <xsl:template match="css:media" mode="css:gather" as="xs:string*">
    <xsl:param name="indent"
      select="$css:empty-string" as="xs:string" tunnel="yes" />

    <!--* generate a CSS media query if needed *-->
    <xsl:if test="@when">
      <xsl:value-of select=" '@media (' || @when || ') {&#xa;' " />
    </xsl:if>

    <xsl:apply-templates mode="#current" select="css:*">
      <!--* indent the children *-->
      <xsl:with-param name="indent" select="$indent || '  '" tunnel="yes" />
    </xsl:apply-templates>

    <xsl:if test="@when">
      <xsl:value-of select=" '}&#xa;' " />
    </xsl:if>
  </xsl:template>

  <xsl:template match="css:rule" mode="css:gather" as="xs:string*">
    <xsl:param name="indent"
      select="$css:empty-string" as="xs:string" tunnel="yes" />

    <!--* generate a  media query if there is @when *-->
    <xsl:if test="@when">
      <xsl:value-of select=" $indent || '@media (' || @when || ') {&#xa;' " />
    </xsl:if>

    <xsl:value-of
      select="
	(if (@when) then $indent else '') ||
	$indent ||
	@match ||
	' {&#xa;'
    " />

    <xsl:value-of
      select="replace(
	replace(. || '&#xa;', '\s+$', '&#xa;', 'm'),
	'^\s+',
	$indent || '  ',
	'm')" />
    <xsl:value-of select=" $indent || '}&#xa;' " />

    <xsl:if test="@when">
      <xsl:value-of select=" $indent || '}&#xa;' " />
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>

