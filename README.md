# CSS Within

XSLT and optional Java in support of CSS Within, a way to make maintaining CSS much easier when generating HTML.

The problem: CSS gets out of date as your XSLT transformation is edited and changed over time.

The solution: generate the CSS from the XSLT stylesheet.

Problem with this solution: the CSS is in a separate template, so you still edit the XSLT without looking at the CSS.

Solution: put CSS fragments just before each XSLT template.

Problem with this solution: you scroll down in your IDE or text editor so the top of the template is at the top of the screen and you don't see the CSS.

Solution: put the CSS _inside_ the template, right where the element is constructed.

Problem with this solution: the XSLT processor sees the CSS and includes it in the output!

Solution: CSS Within: XSLT to pre-process your stylesheet, writing out CSS files and running your transformation.

Problem with this solution: it sounds too good to be true!

Example:

```
<xsl:template match="para[@role = 'summary']">
  <div class="summary">
    <css:rule match="div.summary">
      <css:use ref="summary-box" />
      color: red;
      background-color: white;
    </css:rule>
    <xsl:apply-templates />
  </div>
</xsl:template>
```
