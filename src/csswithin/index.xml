<page xmlns:xsl="example" xmlns:css="other-example">
  <title>CSS Within</title>
  <section>
    <title>TL;DR</title>
    <p>CSS Within is a way of including CSS styles in code that generates
    Web pages.  It reduces the cost of maintaining and
    refactoring code without adding too much work up front.
    </p>
  </section>
  <section>
    <title>Example</title>
    <p>Here is a simple XSLT example uing CSS Within.</p>
    <example>
      <xsl:template match="item" mode="card">
        <!--* show a thumbnail of the product so people
            * can buy it
            *-->
        <div class="card-thumbnail">
          <css:rule match="div.card-thumbnail">
            border: 1px lightgrey solid;
            border-radius: 1rem;
            padding: 1rem;
          </css:rule>
          <xsl:apply-templates mode="card" />
        </div>
      </xsl:template>
    </example>
    <p>If we need to change the HTML <i>div</i> element, or to
    change its <i>class</i> attribute, we need to change the CSS
    accordingly. If the CSS is in a different file, we might forget;
    if the CSS is outside the template, we might not notice it. So
    here it is, in our face, and we see it and easily keep it up to date.</p>
  </section>
  <section>
    <title>Media Queries</title>
    <p>CSS Media Queries are defined in the CSS Conditionals specification,
    and are very widely used.
    You probably most often use media queries to apply different CSS
    styles for different width <i>viewports</i> (or browser windows,
    or whatever).
    </p>
    <example>
      <css:media when="min-width: 50rem">
        <!--* when the screen  is wider than 50rem,
            * limit the line length but show the sidebar
            *-->
        <css:rule match="main p">
          max-width: 50rem;
        </css:rule>
        <css:rule match="aside">
          vsibility: visible;
        </css:rule>
      </css:media>
    </example>
    <p>When you have a really simple media query you can combine
    it with a <i>css:rule</i> like this:</p>
    <example>
      <css:rule match="aside" when="min-width: 50rem">
        visibility: visible;
      </css:rule>
    </example>
    <p>This will generate a CSS media query with one rule in it:</p>
    <example>
      @media (min-width: 50rem) {
        aside {
          visibility: visible;
        }
      }
    </example>
  </section>
  <section>
    <title>Using CSS Within</title>
    <p>Add CSS rules to your stylesheet in templates wherever you like.</p>
    <p>If you run XSLT as normal, the CSS elements will look to the XSLT
    processor like direct element constructors and they will appear
    in the output just like any other markup in your templates.
    You can then remove them with <i>css.xslt</i> perhaps using
    <i>fn:transform()</i>.</p>
    <p>There is a browser extension for Saxon that will make the
    CSS elements be treated as XSLT instructions, and their content
    will vanish.</p>
    <p>Finally, you need to generate the CSS by reading the XSLT itself,
    either using the browser extension or with <i>css.xslt</i>.</p>
  </section>
  <section>
    <title>Advanced usage</title>
    <p>It might happen that you generate the same element from multiple
    templates. You should of cource consider refactoring so that any
    given element/class combination is only generated in one place (this
    is called DRY, Don’t Repeat Yourself),
    but life doesn’t always work that way.
    </p>
    <p>You can instead put a <i>css:rule</i> element in multiple places.
    Give the first one a <i>name</i> attribute with a value like
    “Victoria” and put the CSS styles into it, and make others be empty,
    but with a <i>ref="Victoria"</i> attribute. The <i>name</i> attribute
    will alert you that the style is being used in multiple places.</p>
    <p>You can also define a group of CSS properries and include them
    wherever you want:</p>
    <example>
      <css:group name="font">
        font-family: "Argyle Socks", "Garamond", serif;
        line-height: 1.35;
      </css:group>
      . . .
      <css:rule match="p.abstract">
        <css:group ref="font" />
        font-size: 1.2rem;
      </css:rule>
    </example>
    <p>This will generate:</p>
    <example>
      p.abstract {
        font-family: "Argyle Socks", "Garamond", serif;
        line-height: 1.35;
        font-size: 1.2rem;
      }
    </example>
  </section>
  <section>
    <title>Generating the CSS</title>
    <p>Use a <i>css:stream</i> element to direct output to a CSS file:</p>
    <example>
      <css:stream href="marvin.css" name="julia" />
    </example>
    <p>Here, <i>marvin.css</i> must be a constant string currently.
    If the optional <i>name</i> attribute is given, then only CSS
    media qeries and rules with <i>stream="julia"</i> (or whatever name
    you choose) will go to that file. Any unwritten rules will generate
    an error if you have declared a stream, so if you want a rule
    not to be written out, make a stream with <i>save="no"</i>.
    </p>
  </section>
</page>
