<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet
  version="3.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:file="http://expath.org/ns/file"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:lq="http://www.barefootliam.org/"
  xmlns:css="http://words.barefootliam.org/xml/css/ns"
  xmlns:map="http://www.w3.org/2005/xpath-functions/map"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="css file h map xsl lq"
  extension-element-prefixes="css"
  >

  <!--* include this stylesheet to generate CSS file from embedded
      * css:media and css:rule elements.
      * css:rule match="css selector"
      *   css rule block with no curly braces at start/end
      * css:media when="media query"
      *   css:rule elements
      *-->

  <!--* internal-use-only is set by this stylesheet; don't supply it *-->
  <xsl:param name="internal-use-only" as="xs:string?" select=" () " />

  <!--* CSS styles: global *-->

  <xsl:variable name="here-hack" as="element(*)"><boy /></xsl:variable>
  <xsl:variable name="here" select="doc(base-uri($here-hack))" />

  <xsl:variable name="css-header" expand-text="no" as="xs:string">
@font-face {
font-family: 'IM Fell English PRO';
src: url('fonts/imfellenglishpro.eot');
src: url('fonts/imfellenglishpro.eot') format('embedded-opentype'),
     url('fonts/imfellenglishpro.woff2') format('woff2'),
     url('fonts/imfellenglishpro.woff') format('woff'),
     url('fonts/imfellenglishpro.ttf') format('truetype'),
     url('fonts/imfellenglishpro.svg') format('svg');
}

@font-face {
font-family: 'IM Fell English PRO';
src: url('fonts/imfellenglishproitalic.eot');
src: url('fonts/imfellenglishproitalic.eot') format('embedded-opentype'),
     url('fonts/imfellenglishproitalic.woff2') format('woff2'),
     url('fonts/imfellenglishproitalic.woff') format('woff'),
     url('fonts/imfellenglishproitalic.ttf') format('truetype'),
     url('fonts/imfellenglishproitalic.svg') format('svg');
     font-style: italic;
}

    body {
      background: #FFFFFF;
      color: #300;
      font-size: 13pt; /* changing this requires changing div.ie too */
      line-height: 18.5pt; /* changing this requires changing div.ie too */
      font-family: "IM FELL English PRO", "Sorts Mill Goudy", "Adobe Caslon Pro", "Adobe Caslon Std", "Adobe Caslon", "Caslon Pro", "Caslon", "Adobe Caslon", "ACaslon", "Garamond", "Palatino Linotype", "Palatino", "Book Antiqua", serif;

      margin-right: 0;
      margin-left: 1em;
      margin-top: 0;
      padding-top: 0;
      padding-right: 0;
      -moz-font-feature-settings: "clig=1,dlig=1,onum=1,kern=1,liga=1,calt=1";
      -moz-font-feature-settings: "kern" 1, "liga" 1, "calt" 1, "clig" 1,"dlig" 1,"onum";
      -ms-font-feature-settings: "kern" 1, "liga" 1, "calt" 1, "clig" 1,"dlig" 1,"onum";
      font-feature-settings: "kern" 1, "liga" 1, "calt" 1, "clig" 1,"dlig" 1,"onum";
      text-rendering: optimizeLegibility; /* needed for webkit? */
      -webkit-font-feature-settings: "kern" 1, "liga" 1, "calt" 1, "clig" 1,"dlig" 1,"onum";
      -o-font-feature-settings: "kern" 1, "liga" 1, "calt" 1, "clig" 1,"dlig" 1,"onum";
      font-feature-settings: "kern" 1, "liga" 1, "calt" 1, "clig" 1,"dlig" 1,"onum";
      hyphens: auto;
      hyphenate-limit-lines: 3;
      hyphenate-limit-chars: 6 2 2
      hyphenate-limit-zone: 0.5em;
      hyphenate=limit-last: always;
    }

    /* unused i think */
    p.indent {
      margin: 0;
      padding: 0;
      margin-left: 3em;
      font-size: 90%;
      line-height: 95%;
    }

    h1 {
      font-size: 100%;
      line-height: 120%;
      padding-bottom: 0;
      margin-bottom: 0;
      /* color: #8e8d80; same as nav images */
      font-weight: bold;
    }

    h2 {
      font-size: 100%;
      line-height: 120%;
      padding-bottom: 0;
      margin-bottom: 0;
      font-style: normal;
      font-weight: bold;
    }

    .inline {
      display: inline;
    }

    /* unused? */
    p.textcite {
      text-align: right;
      font-variant: small-caps;
      letter-spacing: 1px;
      margin-top: 0;
    }

    div.body-and-ad {
	/*
        display: grid;
	grid-template-columns: 4fr 1fr;
	*/
    }

    p {
      margin-top: 0;
      margin-bottom: 0.5em;
      text-indent: 1em; 
    }

    p:first { 
      text-indent: 0;
    }

    a {
      /* accessibility: need to fix this */
      text-decoration: none;
    }

    a:link {
      color: #813f3f;
    }
    a:hover {
      color: #090;
      background: #fdf4e0;
    }
    a:visited {
      color: #f99;
    }
    a {
      color: #090;
    }
  </xsl:variable>

  <!--* CSS module *-->

  <xsl:mode name="css" on-no-match="fail" />
  <xsl:mode name="remove-css"  on-no-match="shallow-copy" />
  <xsl:template match="css:*" mode="remove-css" />

  <xsl:function name="css:remove-css">
    <xsl:param name="input" />

    <xsl:apply-templates mode="remove-css" select="$input" />
  </xsl:function>

  <xsl:function name="css:writefile" as="item()*">
    <xsl:param name="uri" as="xs:string"/>
    <xsl:param name="contents" as="item()*" />

    <xsl:choose>
      <xsl:when test="empty($uri) or ($uri eq '')">
        <xsl:sequence select=" $contents " />
      </xsl:when>
      <xsl:when test="$uri eq base-uri($here-hack)" >
        <xsl:message>Overwriting me</xsl:message>
	<xsl:copy-of select="$contents" />
      </xsl:when>
      <xsl:when test="$uri eq 'output'">
        <xsl:sequence select=" $contents " />
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="matches($uri, '.*[^/:]/[^/]+')">
          <!--* we need to create the directory *-->
          <xsl:variable name="dir" select="replace($uri, '/[^/]*$', '')"/>
          <xsl:variable name="result" select="file:create-dir($dir)" />
          <xsl:if test="$result">
            <xsl:message terminate="yes"
              select="concat('file:create-dir(', $dir, ') for ', $uri, ' failed: ', $result)"/>
          </xsl:if>
        </xsl:if>

	<xsl:value-of
	  select="file:write-text(
	    $uri,
	    serialize(
	      css:remove-css($contents),
	      map {
		'html-version' : 5,
		'indent' : false(),
		'method': if (matches($uri, '\.html$')) then 'html' else 'text'
	      }
	  )
	)" />

      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:template use-when="false()" match="document-node()[empty($internal-use-only)]" >
    <xsl:sequence select='
      map:for-each(
	transform(
	  map {
	    "stylesheet-location" : base-uri($here-hack),
	    "source-node" : . ,
	    "post-process" : css:writefile#2,
	    "delivery-format" : "document",
	    "stylesheet-params" : map {
	      QName("", "internal-use-only") : "provided"
	    }
	  }
	),
	function(
	  $key as xs:anyAtomicType,
	  $value as item()*
	) as item()* {
	  if ($key eq "output") then $value
	  else css:writefile($key, $value)
	}
      )
    '/>
  </xsl:template>

  <xsl:template match="/" mode="css">
    <xsl:apply-templates select="*" mode="css" />
  </xsl:template>

  <xsl:template name="make-css">
    <xsl:result-document method="text" href="out/entry.css">
      <xsl:sequence select="$css-header" />

      <xsl:apply-templates select="$here" mode="css" />
    </xsl:result-document>
  </xsl:template>

  <xsl:template match="css:*/text()" mode="css">
    <!--* for otherwise uncaught content *-->
    <xsl:sequence select="lq:css-add-prefixes(.)" />
  </xsl:template>

  <xsl:function name="lq:css-add-prefixes" as="xs:string?">
    <xsl:param name="input" as="xs:string" />

    <!--* NOTDONE !FIXME! *-->
    <xsl:sequence select="$input" />
  </xsl:function>


  <xsl:template match="css:rule[@ref]" mode="css" as="xs:string?">
    <!--* A css:rule element with a ref attribute augments
	* CSS property/value pairs contained in the referenced
	* css:rule element;A it's an error if the ref attribute
	* does not have a value that appears as a name of a
	* css:rule element.  (it can actually be the same element
	* if you like!)
	*-->
    <!--* the referent must match... *-->
    <xsl:if test="not(//css:rule[@name = current()/@ref])">
      <xsl:message terminate="yes" expand-text="yes">
	css:rule referring to {@ref} and match={@match} found,
	but there is no css:rule with name="{@ref}"
      </xsl:message>
    </xsl:if>
    <!--* Now process any content, to supplement other rules for
	* the same selector.
	*-->
    <xsl:if test="text()">
      <xsl:next-match/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="css:media" mode="css">
    <!--* generate a media query if needed: *-->
    <xsl:if test="@when">
      <xsl:sequence select=" '@media (' || @when || ') {&#xa;' " />
    </xsl:if>

    <xsl:apply-templates mode="css" />

    <xsl:if test="@when">
      <xsl:sequence select=" '}&#xa;' " />
    </xsl:if>
  </xsl:template>

  <xsl:template match="css:rule" mode="css" as="xs:string">
    <!--* generate a media query if needed: *-->
    <xsl:if test="@when">
      <xsl:sequence select=" '@media (' || @when || ') {&#xa;' " />
    </xsl:if>

    <xsl:variable name="NL" as="xs:string" select="codepoints-to-string(10)" />

    <xsl:variable name="rule" as="xs:string"
      select="@match || ' {' || . || '}' || $NL " />
    <xsl:sequence select="lq:css-add-prefixes($rule)" />

    <xsl:if test="@when">
      <xsl:sequence select=" '}&#xa;' " />
    </xsl:if>

    <!--* Note: styles after a media query are ignored in old versions
	* of Safari i think, maybe if the selector is the same?
	*-->
  </xsl:template>

  <xsl:template match="*[
    not( namespace-uri(.) eq 'http://words.barefootliam.org/xml/css/ns' )
    ]" mode="css" priority="1000">

    <xsl:apply-templates select="*" mode="css" />
  </xsl:template>

  <xsl:template match="css:rule" expand-text="yes">
    <xsl:message terminate="yes">skip elemement css:{local-name(.)}</xsl:message>
  </xsl:template>

</xsl:stylesheet>
<!--*
vi:sw=2:
  *-->

