package com.delightfulcomputing.css;

import net.sf.saxon.event.*;
import net.sf.saxon.event.Receiver;
import net.sf.saxon.event.ReceiverOptions;
import net.sf.saxon.expr.Expression;
import net.sf.saxon.expr.SimpleExpression;
import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.style.ExtensionInstruction;
import com.saxonica.xsltextn.ExtensionElementFactory;
import net.sf.saxon.style.Compilation;
import net.sf.saxon.style.ComponentDeclaration;
import net.sf.saxon.value.EmptySequence;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.trans.XPathException;

public class SaxonCSS extends ExtensionInstruction
{
    @Override
    public void prepareAttributes()
	throws XPathException
    {
	String rule = getAttributeValue("match");
	if (rule == null) {
	    // System.err.println("prepare but no match");
	} else {
	    // System.err.println("prepare for match = "  + rule);
	}
    }

    @Override
    public void validate(ComponentDeclaration decl)
	throws XPathException
    {
	// System.err.println("validate");
	super.validate(decl);
    }

    @Override
    public Expression compile(Compilation compilation, ComponentDeclaration decl)
	throws XPathException
    {
	SaxonCSSInstruction css = new SaxonCSSInstruction();

	// System.err.println("compile\n");
	return (Expression) css;
    }

    /* http://www.saxonica.com/documentation/#!extensibility/instructions */

    @Override
    public boolean mayContainSequenceConstructor()
    {
	return true;
    }

    private static class SaxonCSSInstruction
	    extends SimpleExpression {

	public SaxonCSSInstruction()
	{
	    Expression[] subexpr = { };
	    // System.err.println("make a new SaxonCSSInstruction\n");
	    setArguments(subexpr);
	}

	@Override
	public int getImplementationMethod()
	{
	    //return Expression.PROCESS_METHOD;
	    return 1;
	    // choices are evaluateItem() iterate() or process()
	    // but the differences are not really explained,
	    // nor which constants to use for which.

	}

	@Override
	public String getExpressionType()
	{
	    return "css:rule";
	}

	@Override
	public Sequence<?> call(XPathContext ctx, Sequence[] arguments)
	throws XPathException
	{
	    // System.err.println("call\n");
	    return (Sequence<?>) EmptySequence.getInstance();
	}

    }
}


