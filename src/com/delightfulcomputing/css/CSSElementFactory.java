package com.delightfulcomputing.css;
import com.saxonica.xsltextn.ExtensionElementFactory;
import net.sf.saxon.style.StyleElement;

public class CSSElementFactory
    implements ExtensionElementFactory
{
    @Override
    public  Class<? extends StyleElement> getExtensionClass(String elementName)
    {
	// System.err.println("factory called for " + elementName);
	if (elementName.equals("rule")) {
	    return com.delightfulcomputing.css.SaxonCSS.class;
	}
	if (elementName.equals("media")) {
	    return com.delightfulcomputing.css.SaxonCSS.class;
	}
	// default:
	return com.delightfulcomputing.css.SaxonCSS.class;
	// System.err.println("unknown element " + elementName + "\n");
	// return null;
    }
}
