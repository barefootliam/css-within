package com.delightfulcomputing.css;

import net.sf.saxon.event.*;
import net.sf.saxon.expr.*;
import net.sf.saxon.om.AxisInfo;
import net.sf.saxon.om.NodeInfo;
import net.sf.saxon.om.Sequence;

import net.sf.saxon.style.Compilation;
import net.sf.saxon.style.ComponentDeclaration;
import net.sf.saxon.style.ExtensionInstruction;
import net.sf.saxon.trans.SaxonErrorCode;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.tree.iter.AxisIterator;
import net.sf.saxon.type.Type;
import net.sf.saxon.value.AtomicValue;
import net.sf.saxon.value.EmptySequence;
import net.sf.saxon.value.Whitespace;

import net.sf.saxon.expr.SimpleExpression;
import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.style.ExtensionInstruction;
import com.saxonica.xsltextn.ExtensionElementFactory;
import net.sf.saxon.style.Compilation;
import net.sf.saxon.style.ComponentDeclaration;
import net.sf.saxon.value.EmptySequence;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.trans.XPathException;

public class SaxonCSSGather extends ExtensionInstruction
{
    static final private String MYNAMESPACE =
	"http://words.barefootliam.org/xml/css/ns";

    /* <css:gather/> in the XSLT is replaced at compile-time
     * by the text content of all css:rule and css:media elements.
     */
    @Override
    public void prepareAttributes()
	throws XPathException
    {
	String stream = getAttributeValue("stream");
	if (stream == null) {
	} else {
	    // TODO:  process only a single stream
	}
    }

    @Override
    public void validate(ComponentDeclaration decl)
	throws XPathException
    {
	// System.err.println("validate");
	super.validate(decl);
    }

    private String recursiveGather(
	    AxisIterator iter
	    )
    {
	String  result = "";
	AxisIterator childIterator = iterateAxis(AxisInfo.DESCENDANT);

	NodeInfo child = childIterator.next();
	while (child != null) {
	    if (child instanceof StyleElement) {
		StructuredQName n = child.getObjectName();

		String uri = n.getURI();
		if (uri.equals(MYNAMESPACE)) {
		    String name = n.getLocalPart();
		    if (!n.equals("gather")) {
			/*
			result += handleElement(
			);
			*/
		    }
		}
	    }
	}
    }

    @Override
    public Expression compile(Compilation compilation, ComponentDeclaration decl)
	throws XPathException
    {
	SaxonCSSInstruction css = new SaxonCSSInstruction();
	// This is where we do the actual work and store it until
	// runtime

	System.err.println("compile gather\n");
	AxisIterator getUpThereIterator = iterateAxis(AxisInfo.ANCESTOR);
	NodeInfo root = getUpThereIterator.next();
	{
	    NodeInfo tmp = root;
	    while (root != null) {
		tmp = root;
		root = getUpThereIterator.next();
	    }
	    root = tmp;
	}

	String styles = "";


	return (Expression) css;
    }

    /* http://www.saxonica.com/documentation/#!extensibility/instructions */

    @Override
    public boolean mayContainSequenceConstructor()
    {
	return true;
    }

    private static class SaxonCSSInstruction
	    extends SimpleExpression {

	public SaxonCSSInstruction()
	{
	    Expression[] subexpr = { };
	    // System.err.println("make a new SaxonCSSInstruction\n");
	    setArguments(subexpr);
	}

	@Override
	public int getImplementationMethod()
	{
	    //return Expression.PROCESS_METHOD;
	    return 1;
	    // choices are evaluateItem() iterate() or process()
	    // but the differences are not really explained,
	    // nor which constants to use for which.

	}

	@Override
	public String getExpressionType()
	{
	    return "css:rule";
	}

	@Override
	public Sequence call(XPathContext ctx, Sequence[] arguments)
	throws XPathException
	{
	    // System.err.println("call\n");
	    return EmptySequence.getInstance();
	}

    }
}


