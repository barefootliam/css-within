#! /bin/sh

# no gradle script yet, sorry.
export CLASSPATH=/home/lee/packages/saxon-ee/saxon9ee.jar
set -x
/usr/lib/jvm/java-1.8.0/bin/javac CSSElementFactory.java SaxonCSS.java &&
  jar cvf ../../../SaxonCSS.jar *.class
