# CSS Within: Quick Start

First, clone this small repository, or download the files and unpack them.
This gives you
* a docs folder including this file (README.md);
* a lib folder containing various XSLT files;
* a java folder containing an extension to Saxon 9 or later (use
  of this is optional);
* a samples folder containing one or more examples.

The simplest way to get started is to look at the samples. The TL;DR is that
you choose one of three approaches and then add CSS elements to your
stylesheet.

If you already have a CSS file, you'll want to split it up and put
it into individual templates.  Although this task could be largely automated,
the act of going through the CSS file and doing this by hand is quick and often
turns up problems with unused CSS or other errors, so for small projects it's
worth doing. In the future, CSS Within may include a program to help with this.

This document describes the various CSS elements you can use.

## Namespace
The CSS elements are all in a namespace that you need to declare on
your xsl:stylesheet element. Add,
```
  xmlns:css="http://words.barefootliam.org/xml/css/ns"
  exclude-result-prefixes="css"
```
to the top-level xsl:stylesheet, xsl:package or xsl:transform element.

## css:rule

```
<css:rule match=" _selector-list_ " [stream="_id_"]>
    text() | css:use
</css:rule>
```

This is the simplest element. Most of the time it just contains
CSS properties with their values. The _selector-list_ is a
comma-separated list of CSS selectors; the result of
```
<css:rule match="body div.socks span.weave, span.hole">
  color: blue;
</css:rule>
```
will be CSS like this:
body div.socks span.weave, span.hole {
  color: blue;
}
```
Notice that there are no curly braces in the css:rule element. If ever
you find yourself putting curly braces inside an element in the css
namespace, you need to stop and ask why. Text value templates are _not_
supported, because the contents are generated by a pre-processor. If you
need to include the values of variables or the results of your XSLT,
see _css:generate_ later in this document.

The optional _stream_ attribute is for supporting multipe output files:
give each one a name, such as _screen_ or _print_ for example, and then
use the corresponding name on css elements. See also _css:generate_.
The value is a space-separated list of stream names, each an NCName
(no colons), or, instead of a list, the single value #all or #ALL to 
write to all files.

If the stream is omitted, the rules go to the
default (unnamed) stream, and a css:generate element with no _stream_
attribute will produce the collated results.

You can also use a _ref_ attribute on a _css:rule_ element; no output
is generated, but you can use it as a reminder:
```
<p class="breadcrumb">
  <css:rule ref="css-breadcrumb-rule" />
  <a href="../">Up</a>
</p>
```
There must be a _css:rule_ element with a matching _name_ attribute
for this to work. The _name_ attribute is otherwise ignore on
_css:rule_ elements; they still generate output, so you only get
one copy of the CSS rule however many times you refer to it.

## css:generate
The generated XSS ends up here. If you are using CSS 3, you can use
_xsl:result-document_ or _file:write()_ to put it in a file.

```
<xsl:result-document href="web.css" method="text">
  <css:generate />
</xsl:result-document>
```

If you need to generate multiple CSS files, add a _stream_ attribute:
```
<xsl:result-document href="web.css" method="text">
  <css:generate />
</xsl:result-document>
<xsl:result-document href="print.css" method="text">
  <css:generate stream="print" />
</xsl:result-document>
```

In this case you will also need to add _stream="print"_ attributes to any
css elements that are specific to print, or _stream="#ALL"_ to output
to all streams.

If you like, you can put _css:generate_ inside an _xsl:variable_ with
_as="xs:string__ and process it for example with _fn:replace()_.
See also _css:header_ for another, better, approach.

There must be a _css:generate_ element with a stream name for each
stream you use in the document.

## css:header
The css:header element can appear anywhere but you should only have
one of them for each CSS stream you plan to use. Normally it's at the
top level of the stylesheet.

The contents are
copied literally to the start of the CSS file. Currently, it's a good
idea to add _xsl:expand-text="no" to this so it can contain things like
at-rules with curly braces in them:

```
<css:header stream="print" expand-text="no">
    @page {
	margin: 2pc;
    }
</css:header>
```

You can also include CSS custom properties in the CSS header:
```
<css:header stream="print" expand-text="no">
  <css:rule match=":root">
    --foreground-colour: xsl-expression($fg));
    --alert-border-width: xsl-expression($pageWidth * 0.02);
  </css:rule>
</css:header>
```

This is not yet implemented and may change slightly; the idea is
that the CSS header appears in content where your css:generate appears,
probaby in a _css:text expand-text="yes"_ but with curly braces quoted
automatically and _xsl-expression()_ changed to curly braces.

## css:media
Use this to generate CSS media  queries, as defined in the
CSS Conditional specification:
```
<css:media when="min-width: 1800px" [stream=" _stream_ ]">
  <css:rule match="p">
    max-width: 50em;
  </css:rule>
</css:media>
```
You can include _css:media_ elements anywhere in your stylesheet.

The _stream_ attribute is the same as for _css:rule_.

## css:define and css:use

The _css:use_ element takes a _ref=" name "_ attribute and is replaced
by the _contents_ of the CSS element with a matching _name_ attribute.
The match is by string equality and is case sensitive.

You can use a _css:define_ element with a _name_ attribute if you like;
this will not generate any output itself, but its content can be
reused with a ref= mechanism.

```
<css:define name="css-breadcrumb">
  display: block;
  margin-left: 48px;
</css:define>

<css:rule match="p.breadcrumb">
  <css:use ref="css-breadcrumb" />
  margin-right: 90px;
</css:rule>
```

This is different from using _css:rule_ with a _ref_ attribute because
only the _contents_ of the _css:define_ is copied.

## css:font
Not currently implemented. Use _css:header_.

